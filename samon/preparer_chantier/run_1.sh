chantier=$1
annee=$2


echo "Conversion des images en tif"
find ${chantier}/pvas -maxdepth 1 -name "*jp2" -exec basename {} .jp2 ';' | parallel -I% --max-args 1 gdal_translate ${chantier}/pvas/%.jp2 ${chantier}/pvas/%.tif
rm -f ${chantier}/pvas/*.jp2

echo "Mise à jour du répertoire Analyse_plan_vol"
python visualiser_chantier.py --input_xml ${chantier}/orientation/ --filtre_images 1 --chantier ${chantier}

echo "Téléchargement de la BD Ortho et du MNT"
python recuperer_BD_Ortho.py --metadata ${chantier}/Analyse_Plan_Vol/ --annee ${annee} --chantier ${chantier}/
rm -rf ${chantier}/ortho_temp

echo "Conversion des dalles d'ortho en tif"
find ${chantier}/ortho -maxdepth 1 -name "*jp2" -exec basename {} .jp2 ';' | parallel -I% --max-args 1 gdal_translate ${chantier}/ortho/%.jp2 ${chantier}/ortho/%.tif
rm -f ${chantier}/ortho/*jp2

echo "Création d'un vrt sur les dalles d'ortho"
gdalbuildvrt ${chantier}/ortho/ortho.vrt ${chantier}/ortho/OR*.tif

echo "Extraction des dalles de MNT"
gzip -d ${chantier}/mnt/*.gz
echo "Création d'un vrt sur les dalles de MNT"
gdalbuildvrt ${chantier}/mnt/mnt.vrt ${chantier}/mnt/*.asc

mkdir ${chantier}/raf
cp  raf/raf2020_2154.tif ${chantier}/raf/