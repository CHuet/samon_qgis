import os
from lxml import etree
from osgeo import ogr
from osgeo import osr
import argparse

parser = argparse.ArgumentParser(description="Visualisation de la position approximative des chantiers")
parser.add_argument('--chantier', help='Fichier xml du chantier')
parser.add_argument('--input_xml', help='Fichier xml du chantier')
parser.add_argument('--filtre_images', help='Filtrer par les images présentes dans le répertoire')
args = parser.parse_args()


def findEPSG(root):
    projection = root.find(".//projection").text.strip()
    dictionnaire = {
        "UTM_WGS84_f01sud":32701,
        "UTM_WGS84_f05sud":32705,
        "UTM_WGS84_f06sud":32706,
        "UTM_WGS84_f07sud":32707,
        "UTM_WGS84_f12nord":32612,
        "UTM_WGS84_f20nord":32620,
        "UTM_WGS84_f21nord":32621,
        "UTM_WGS84_f22nord":32622,
        "UTM_WGS84_f30nord":32630,
        "UTM_WGS84_f31nord":32631,
        "UTM_WGS84_f32nord":32632,
        "UTM_WGS84_f33nord":32633,
        "UTM_WGS84_f34nord":32634,
        "UTM_WGS84_f36nord":32636,
        "UTM_WGS84_f38sud":32738,
        "UTM_WGS84_f39sud":32739,
        "UTM_WGS84_f40sud":32740,
        "UTM_WGS84_f42sud":32742,
        "UTM_WGS84_f43sud":32743,
        "UTM_WGS84_f58sud":32758,
        "Lambert93":2154,
    }

    EPSG = dictionnaire[projection]
    with open(os.path.join(args.chantier, "Analyse_Plan_Vol", "EPSG.txt"), "w") as f:
        f.write(str(EPSG))
    

    print("L'EPSG du chantier est {}".format(EPSG))

    return EPSG


def get_ta_xml(repertoire):
    fichier = [i for i in os.listdir(repertoire) if i[-4:]==".XML"]
    return os.path.join(repertoire, fichier[0])

def lecture_xml(path):
    #Récupère pour chaque cliché le nom de l'image et l'emprise donnée par le fichier xml
    tree = etree.parse(path)
    root = tree.getroot()

    images = []
    
    for cliche in root.getiterator("cliche"):
        image = {}
        image["nom"] = cliche.find("image").text.strip()
        polygon2d = cliche.find("polygon2d")
        x = polygon2d.findall("x")
        y = polygon2d.findall("y")
        x = [float(i.text) for i in x]
        y = [float(i.text) for i in y]
       
        image["x"] = x
        image["y"] = y
        images.append(image)

    EPSG = findEPSG(root)

    return images, EPSG


def save_shapefile(liste_images, images, path_emprise_chantier, EPSG):
    #Sauvegarde les emprises dans un fichier shapefile

    driver = ogr.GetDriverByName("ESRI Shapefile")

    ds = driver.CreateDataSource(path_emprise_chantier)
    srs =  osr.SpatialReference()
    srs.ImportFromEPSG(EPSG)

    layer = ds.CreateLayer("line", srs, ogr.wkbPolygon)

    nameField = ogr.FieldDefn("nom", ogr.OFTString)
    layer.CreateField(nameField)

    featureDefn = layer.GetLayerDefn()

    xmin = 1e15
    ymin = 1e15
    xmax = -1e15
    ymax = -1e15


    for image in images:
        if (args.filtre_images=="1" and image["nom"] in liste_images) or args.filtre_images=="0":
            ring = ogr.Geometry(ogr.wkbLinearRing)
            for i in range(len(image["x"])):
                ring.AddPoint(image["x"][i], image["y"][i])
                xmin = min(xmin, image["x"][i])
                ymin = min(ymin, image["y"][i])
                xmax = max(xmax, image["x"][i])
                ymax = max(ymax, image["y"][i])
            ring.AddPoint(image["x"][0], image["y"][0])

            poly = ogr.Geometry(ogr.wkbPolygon)
            poly.AddGeometry(ring)
                
            feature = ogr.Feature(featureDefn)
            feature.SetGeometry(poly)
            feature.SetField("nom", image["nom"])
            layer.CreateFeature(feature)

            feature = None

    ds = None
    return [xmin, ymin, xmax, ymax]


def getListeImages():
    #Récupère la liste des images présentes dans le dossier
    liste_fichiers = os.listdir(os.path.join(args.chantier,"pvas"))
    liste_images = []
    for fichier in liste_fichiers:
        if fichier[-4:] == ".tif" or fichier[-4:] == ".jp2":
            liste_images.append(fichier.split(".")[0])
    return liste_images

def get_bbox(liste_images, images):
    #Sauvegarde les emprises dans un fichier shapefile

    xmin = 1e15
    ymin = 1e15
    xmax = -1e15
    ymax = -1e15

    for image in images:
        if image in liste_images:
            for i in range(len(image["x"])):
                xmin = min(xmin, image["x"][i])
                ymin = min(ymin, image["y"][i])
                xmax = max(xmax, image["x"][i])
                ymax = max(ymax, image["y"][i])

    return [xmin, ymin, xmax, ymax]


def save_bbox(bbox):
    
    with open(os.path.join(args.metadata, "bbox.txt"), "w") as f:
        for i in bbox:
            f.write("{}\n".format(i))

def save_bbox(bbox):
    
    with open(os.path.join(args.chantier, "Analyse_Plan_Vol", "bbox.txt"), "w") as f:
        for i in bbox:
            f.write("{}\n".format(i))

if not(os.path.exists(os.path.join(args.chantier, "Analyse_Plan_Vol"))):
    os.mkdir(os.path.join(args.chantier, "Analyse_Plan_Vol"))
if not(os.path.exists(os.path.join(args.chantier, "ortho"))):
    os.mkdir(os.path.join(args.chantier, "ortho"))
path_emprise_chantier = os.path.join(args.chantier, "Analyse_Plan_Vol", "chantier.shp")
path_emprise_chantier_merged = os.path.join(args.chantier, "Analyse_Plan_Vol", "chantier_merged.shp")
path_xml = get_ta_xml(args.input_xml)



#Récupère la liste des images présentes dans le dossier
liste_images = getListeImages()

#Récupère les emprises indiquées dans le fichier de métadonnées xml du chantier 
images, EPSG = lecture_xml(path_xml)

#Sauvegarde dans un fichier shapefile les emprises seulement pour les images présentes dans le dossier
bbox = save_shapefile(liste_images, images, path_emprise_chantier, EPSG)


save_bbox(bbox)