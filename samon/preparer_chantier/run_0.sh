chantier=$1



mkdir ${chantier}/pvas
python visualiser_chantier.py --chantier ${chantier} --input_xml ${chantier}/orientation/ --filtre_images 0

echo "Choisissez les images dont vous avez besoin dans store-ref/orthophotos/images orientées/[département]/[année]/..._C_20_JP2_E095"
echo "Mettez-les dans le répertoire pvas"
echo "Lancer le script run_1.sh"