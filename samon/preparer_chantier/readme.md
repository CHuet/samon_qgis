### Choisir les pvas

* Récupérer le fichier TA : store-ref/orthophotos/images orientées/[département]/[année]/..._AERO/AERO/.../..._adjust.XML
* Mettre le fichier TA dans le répertoire [chantier]/orientation

Lancer le script run_0.sh. Ce script crée un répertoire Analyse_plan_vol avec les emprises au sol de toutes les pvas.
Copier dans le répertoire pvas toutes les images dont vous avez besoin

Lancer le script run_1.sh. Ce script met à jour Analyse_plan_vol et récupère la bd ortho et le mnt de la zone.

Ajouter la grille raf