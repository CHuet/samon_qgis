from goutieres.calculGoutieres import CalculGoutieres


pva = "/home/CHuet2/Documents/monoscopie/chantiers/05_2022/pvas"
#shapefile = "/home/CHuet2/Documents/monoscopie/chantiers/05_2022/intersection_goutieres"
#shapefile = "/home/CHuet2/Documents/monoscopie/chantiers/05_2022/frameFieldLearning/inferences_frame_field_learning_association"
shapefile = "/home/CHuet2/Documents/monoscopie/chantiers/05_2022/frameFieldLearning/propres_v2_association"
#shapefile = "/home/CHuet2/Documents/monoscopie/chantiers/05_2022/frameFieldLearning/inferences_frame_field_learning_association_regroupees"
ortho = "/home/CHuet2/Documents/monoscopie/chantiers/05_2022/ortho/ortho.vrt"
mnt =  "/home/CHuet2/Documents/monoscopie/chantiers/05_2022/mnt/mnt.vrt"
ta_xml = "/home/CHuet2/Documents/monoscopie/chantiers/05_2022/Orientation/22FD0520_adjust.XML"
resultats = "/home/CHuet2/Documents/monoscopie/chantiers/05_2022/resultats_goutieres"
raf = "/home/CHuet2/Documents/monoscopie/chantiers/05_2022/raf/raf2020_2154.tif"


calculGoutieres = CalculGoutieres(ta_xml, shapefile, mnt, raf, pva, resultats)
calculGoutieres.run()