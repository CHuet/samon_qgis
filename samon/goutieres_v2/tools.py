import os
import shapely

def get_mnt(repertoire):
    fichier = [i for i in os.listdir(repertoire) if i[-4:]==".vrt"]
    return os.path.join(repertoire, fichier[0])

def get_raf(repertoire):
    fichier = [i for i in os.listdir(repertoire) if i[-4:]==".tif"]
    return os.path.join(repertoire, fichier[0])

def get_ta_xml(repertoire):
    fichier = [i for i in os.listdir(repertoire) if i[-4:]==".XML"]
    return os.path.join(repertoire, fichier[0])



def make_valid(polygon):
    polygon = shapely.make_valid(polygon)
    if isinstance(polygon, shapely.MultiPolygon):
        polygones = []
        for p in list(polygon.geoms):
            polygones.append(p)
    elif isinstance(polygon, shapely.Polygon):
        polygones = [polygon]
    elif isinstance(polygon, shapely.LineString):
        polygones = []
    elif isinstance(polygon, shapely.GeometryCollection):
        polygones = []
        for p in list(polygon.geoms):
            if isinstance(p, shapely.Polygon):
                polygones.append(p)
    else:
        print("cas non traité : ", polygon)
        polygones = []
    return polygones