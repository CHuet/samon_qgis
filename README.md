# Samon_QGIS


Répertoire de l'AD Samon


L'objectif de l'AD est de créer un plugin QGis permettant pour chaque clic sur la BD de récupérer les coordonnées 3D du point (en corrigeant notamment le dévers). Pour une présentation générale du processus, voir la page intranet du SIMV : https://ignf.sharepoint.com/sites/SIMV/SitePages/Samon---Saisie-monoscopique-sur-ortho.aspx.

Une extension de l'AD consiste à récupérer en 3D les "goutières", c'est-à-dire les contours des toits de bâtiment.

Deux chantiers tests sont disponibles sur le store-dai (store-dai/pocs/saisie_monoscopique/chantiers_tests) : Mont-Dauphin dans les Hautes-Alpes et Martigné-Briand en Anjou. Contrairement au chantier sur l'Anjou, le chantier de Mont-Dauphin dispose des données du Lidar HD pour contrôler les résultats, ainsi que d'inférences de Frame Field Learning permettant de faire tourner l'algorithme des goutières.


## Plugin QGIS

### Installation

mkdir ~/.local/share/QGIS/QGIS3/profiles/default/python/plugins/samon
cp -r ~/Documents/samon_qgis/samon/*  ~/.local/share/QGIS/QGIS3/profiles/default/python/plugins/samon

Pour installer PySocle :
* cd ~/.local/share/QGIS/QGIS3/profiles/default/python/plugins/samon/monoscopie

On installe la librairie pysocle
* python -m pip install .

Ouvrir le plugin qgis dans qgis :
* Extensions/Installer/Gérer les extensions
* Sélectionner samon

Dans le cas où Pysocle n'est pas installée dans un répertoire où qgis peut la trouver :
Dans le terminal qgis : 
* import sys
* sys.path
Dans un terminal linux :
* pip show pysocle
Puis adapter la ligne suivante :
* cp -r ~/miniforge3/lib/python3.10/site-packages/pysocle ~/.local/share/QGIS/QGIS3/profiles/default/python

Relancer le plugin et installer toutes les autres librairies manquantes

* QGIS/Extensions/Console Python
* import pip
* pip.main(['install', 'shapely'])
* pip.main(['install', 'fiona'])
* pip.main(['install', 'scikit-learn'])
* pip.main(['install', 'plyfile'])
* pip.main(['install', 'tqdm'])


Inutile :
* git clone http://gitlab.forge-idi.ign.fr/SIA/PySocle.git
* cd PySocle/
* nano pyproject.toml
* Ajouter [tool.setuptools.packages]
find = {}
* Modifier la version de Python dans le toml
* python -m build

### Mode d'emploi

* Cliquer sur le bouton recharger l'extension
* Cliquer sur le bouton Samon
* Remplir les différents champs. Les valeurs par défaut de taille de vignettes et de seuil n'ont pas besoin d'être modifiées.
* Pour voir les logs : Vue/Panneaux/Journal des messages/Infos Samon
* La touche h permet d'afficher la fenêtre d'aide avec notamment les raccourcis claviers


## Utiliser Samon en lignes de commande

Dans le cas où l'on veut faire fonctionner Samon sur une liste de points sans avoir à les cliquer dans l'interface QGIS, le script reconstruction_batiment.py indique comment faire


## Reconstruire une zone

Lancer le script reconstruction_batiment.py


## Pour créer un chantier

Voir le readme dans preparer_chantier


### Contrôle LiDAR (optionnel)

Dans samon.py, self.mns_lidar_path = [chemin/du/MNS/LiDAR]

## Pistes d'amélioration

Avec la saisie sur QGIS, ne pas sauvegarder les orthos locales et droites épipolaires, mais les recalculer uniquement lorsque l'on veut le détail (choisir les orthos utiles pour le calcul, cliquer sur une autre ortho...). Cela permettrait de gagner du temps car ce sont les étapes de sauvegarde de fichiers qui sont longues.



## Intersection de goutières

Calcule l'intersection de segments saisis sur les pvas. Les scripts sont dans goutieres_v2. Le répertoire goutieres contient un code obsolète.

Pour le détail des traitements, voir le readme du répertoire goutieres_v2